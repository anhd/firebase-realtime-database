package com.example.android.firebasedatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    TextView mTextFieldCondition;

    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mconditionRef = mRootRef.child("condition");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextFieldCondition = (TextView)findViewById(R.id.textViewCondition);

        EditText inputText = (EditText) findViewById(R.id.messageText);
        inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionID, KeyEvent keyEvent) {
                if (actionID == EditorInfo.IME_ACTION_SEND) {
                    sendMessage();
                }
                return true;
            }
        });
        findViewById(R.id.sendButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
    }
    
        public void sendMessage(){
        EditText textInput = (EditText) findViewById(R.id.messageText);
            String message = textInput.getText().toString();
            if(!message.equals("")){
                Random rand = new Random();
                String author = "TestUser" + rand.nextInt(1000);
                ChatMessage cMessage = new ChatMessage(author, message,"UA");
          String text = cMessage.toString();
                mconditionRef.push().setValue(cMessage);
                textInput.setText("");
            }
    }
    }
